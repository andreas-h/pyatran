from .context import pyatran  # noqa
from pyatran.output.read_inp_pars import \
    read_inp_par, read_xsection_filenames, sanitize_single_param


def test_sanitize_single_param():
    actual = sanitize_single_param("'o4_thalman-volkamer_vac4air_293K.dat'")
    assert actual == 'o4_thalman-volkamer_vac4air_293K.dat'

    assert sanitize_single_param('t') is True
    assert sanitize_single_param('T') is True
    assert sanitize_single_param('f') is False
    assert sanitize_single_param('F') is False

    assert sanitize_single_param('293') == 293
    assert isinstance(sanitize_single_param('293'), int)

    assert sanitize_single_param('293.0') == 293.
    assert isinstance(sanitize_single_param('293.0'), float)
    assert sanitize_single_param('293.5') == 293.5


FN_TEST = 'tests/data/SCE_INP-PARS.OUT_slant_col_384'


def test_read_inp_par():
    actual = read_inp_par(FN_TEST, 'Advanced accuracy control')
    expected = True
    assert actual == expected

    actual = read_inp_par(FN_TEST, 'Include land fluorescence')
    expected = ['off', '../data/flu_land/fluor.dat', 1.]
    assert actual == expected

    actual = read_inp_par(FN_TEST, 'Percent volume concentration')
    expected = [78.084, 20.946, .934, .036]
    assert actual == expected

    actual = read_inp_par(FN_TEST, 'X-section: o4')
    expected = [1, ['o4_thalman-volkamer_vac4air_293K.dat', 293]]
    assert actual == expected

    actual = read_inp_par(FN_TEST, 'Collision-induced absorption')
    assert actual is None


def test_read_xsection_filenames():
    actual = read_xsection_filenames(FN_TEST)

    expected = [['./DATA_IN/bro_fleischmann_vac4air_223K.dat', 223]]
    assert actual['bro'] == expected

    expected = './DATA_IN/o3_serdyuchenko_t-param_vac4air_210K.dat'
    assert actual['o3 UV-NIR'][6][0] == expected

    expected = ['./DATA_IN/O3_serdyuch_poly2_coeff.dat', 245.1, 280.0]
    assert actual['o3_uv, Bass'] == expected
